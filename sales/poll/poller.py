import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()


from sales_rest.models import AutomobileVO


def get_autos():
    print("I'm in pain")
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    print(response)
    content = json.loads(response.content)
    print(content)
    for auto in content["autos"]:
        print(auto)
        AutomobileVO.objects.update_or_create(vin=auto["vin"])
        print(AutomobileVO.objects.all())


def poll():
    while True:
        print("Sales poller polling for data")
        try:
            get_autos()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
