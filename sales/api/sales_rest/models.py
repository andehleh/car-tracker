from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=300, unique=True)
    sold = models.BooleanField(default=False)


class SalesPerson(models.Model):
    salesperson_name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=50)


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)


class SaleRecord(models.Model):
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.PROTECT,
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="salesperson",
        on_delete=models.PROTECT,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.PROTECT,
    )

    price = models.CharField(max_length=20)
