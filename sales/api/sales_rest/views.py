from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, SaleRecord, SalesPerson, Customer
from django.http import JsonResponse
from .encoders import (
    AutomobileVOEncoder,
    SaleRecordEncoder,
    SalesPersonEncoder,
    CustomerEncoder,
)


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        return JsonResponse({"salespeople": salespeople}, encoder=SalesPersonEncoder)
    else:
        content = json.loads(request.body)
        salespeople = SalesPerson.objects.create(**content)
        return JsonResponse(salespeople, encoder=SalesPersonEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_salespeople(request, employee_id=None):
    if request.method == "GET":
        salesperson = SalesPerson.objects.get(id=employee_id)
        return JsonResponse({"salesperson": salesperson}, encoder=SalesPersonEncoder)
    elif request.method == "PUT":
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=employee_id).update(**content)
        salesperson = SalesPerson.objects.get(id=employee_id)
        return JsonResponse(salesperson, encoder=SalesPersonEncoder, safe=False)
    else:
        count, _ = SalesPerson.objects.filter(id=employee_id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customers = Customer.objects.create(**content)
        return JsonResponse(customers, encoder=CustomerEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customers(request, customer_id=None):
    if request.method == "GET":
        customer = Customer.objects.get(id=customer_id)
        return JsonResponse({"customer": customer}, encoder=CustomerEncoder)
    elif request.method == "PUT":
        content = json.loads(request.body)
        Customer.objects.filter(id=customer_id).update(**content)
        customer = Customer.objects.get(id=customer_id)
        return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
    else:
        count, _ = Customer.objects.filter(id=customer_id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_sales(request, id=None):
    if request.method == "GET":
        if id is None:
            sales = SaleRecord.objects.all()
            print(sales)
            return JsonResponse({"sales": sales}, encoder=SaleRecordEncoder)
        else:
            sales = SaleRecord.objects.filter(sales_person=id)
            return JsonResponse({"sales": sales}, encoder=SaleRecordEncoder)
    else:
        content = json.loads(request.body)
        print(content)
        try:
            print("hi")
            if "sales_person" in content:
                sales_person = SalesPerson.objects.get(id=content["sales_person"])
                content["sales_person"] = sales_person
            if "customer" in content:
                customer = Customer.objects.get(id=content["customer"])
                content["customer"] = customer
            if "automobile" in content:
                AutomobileVO.objects.filter(vin=content["automobile"]).update(sold=True)
                automobile = AutomobileVO.objects.get(vin=content["automobile"])
                content["automobile"] = automobile

            print(content)

        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales person not found"}, status=400)
        sale = SaleRecord.objects.create(**content)
        return JsonResponse(sale, encoder=SaleRecordEncoder, safe=False)


@require_http_methods(["GET"])
def api_list_autos(request):
    autos = AutomobileVO.objects.all()
    return JsonResponse({"autos": autos}, encoder=AutomobileVOEncoder)
