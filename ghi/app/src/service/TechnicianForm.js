import { useState, useEffect } from "react";

function TechnicianForm() {
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState({
    technician_name: "",
    employee_number: "",
  });

  const fetchData = async () => {
    const url = "http://localhost:8080/api/technician/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const techniciansUrl = "http://localhost:8080/api/technician/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(techniciansUrl, fetchOptions);
    if (response.ok) {
      setFormData({
        technician_name: "",
        employee_number: "",
      });
    }
  };

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Register a New Technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                value={formData.technician_name}
                onChange={handleChange}
                placeholder="name"
                required
                type="text"
                name="technician_name"
                id="technician_name"
                className="form-control"
              />
              <label htmlFor="fabric">Technician Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.employee_number}
                onChange={handleChange}
                placeholder="number"
                required
                type="text"
                name="employee_number"
                id="employee_number"
                className="form-control"
              />
              <label htmlFor="color">Technician Number</label>
            </div>
            <button
              className="btn btn-primary mx-auto d-block mt-3"
              style={{
                backgroundColor: "#007bff",
                border: "none",
                borderRadius: "5px",
                fontSize: "1.2rem",
                fontWeight: "bold",
                padding: "10px 20px",
              }}
            >
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
