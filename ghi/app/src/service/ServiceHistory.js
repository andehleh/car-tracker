import React, { useState, useEffect } from "react";

function ServiceHistory() {
  const [search, setSearch] = useState("");
  const [vinId, setVinId] = useState([]);

  const fetchVinData = async () => {
    const url = `http://localhost:8080/api/services/history/`;

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setVinId(data.history);
    }
  };

  useEffect(() => {
    fetchVinData();
  }, []);

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const handleSearchSubmit = (event) => {
    event.preventDefault();
    fetchVinData();
  };

  const filteredVinId = vinId.filter((data) => {
    return data.vin.includes(search);
  });

  return (
    <>
      <div className="container">
        <h1 className="mt-4 mb-4">History of Service Appointments</h1>
        <form onSubmit={handleSearchSubmit}>
          <input
            onChange={handleSearch}
            value={search}
            type="search"
            placeholder="Search VIN"
            className="form-control rounded"
            aria-label="Search"
          />
        </form>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Owner</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {filteredVinId.map((data) => {
              const appointmentDate = new Date(data.appointment);
              const appointmentDateString =
                appointmentDate.toLocaleDateString();
              const appointmentTimeString =
                appointmentDate.toLocaleTimeString();
              return (
                <tr key={data.id}>
                  <td>{data.vin}</td>
                  <td>{data.owner}</td>
                  <td>{appointmentDateString}</td>
                  <td>{appointmentTimeString}</td>
                  <td>{data.technician.technician_name}</td>
                  <td>{data.reason}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ServiceHistory;
