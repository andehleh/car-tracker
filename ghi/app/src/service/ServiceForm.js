import { useState, useEffect } from "react";

function ServiceForm() {
  const [services, setServices] = useState([]);
  const [formData, setFormData] = useState({
    vin: "",
    owner: "",
    appointment: "",
    technician: "",
    reason: "",
    finished: false,
  });
  const fetchData = async () => {
    const url = "http://localhost:8080/api/technician/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setServices(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const servicesUrl = "http://localhost:8080/api/services/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(servicesUrl, fetchOptions);
    if (response.ok) {
      setFormData({
        vin: "",
        owner: "",
        appointment: "",
        technician: "",
        reason: "",
        finished: false,
      });
    }
  };

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New Service Appointment</h1>
          <form onSubmit={handleSubmit} id="create-service-form">
            <div className="form-floating mb-3">
              <input
                value={formData.vin}
                onChange={handleChange}
                placeholder="vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="fabric">Vin Number</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.owner}
                onChange={handleChange}
                placeholder="owner"
                required
                type="text"
                name="owner"
                id="owner"
                className="form-control"
              />
              <label htmlFor="color">Car Owner</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.appointment}
                onChange={handleChange}
                placeholder="appointment"
                required
                type="datetime-local"
                name="appointment"
                id="appointment"
                className="form-control"
              />
              <label htmlFor="color">Appointment Time</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={formData.technician}
                onChange={handleChange}
                required
                name="technician"
                id="technician"
                className="form-control"
              >
                <option value="">Select a Technician</option>
                {services.map((technician) => (
                  <option key={technician.id} value={technician.id}>
                    {technician.technician_name}
                  </option>
                ))}
              </select>
              <label htmlFor="technician">Technician</label>
            </div>

            <div className="form-floating mb-3">
              <input
                value={formData.reason}
                onChange={handleChange}
                placeholder="reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="color">Reason</label>
            </div>

            <button
              className="btn btn-primary mx-auto d-block mt-3"
              style={{
                backgroundColor: "#007bff",
                border: "none",
                borderRadius: "5px",
                fontSize: "1.2rem",
                fontWeight: "bold",
                padding: "10px 20px",
              }}
            >
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ServiceForm;
