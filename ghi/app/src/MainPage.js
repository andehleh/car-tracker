import { NavLink } from "react-router-dom";

function MainPage() {
  return (
    <div
      className="bg-image bg-cover position-absolute top-0 start-0"
      style={{
        backgroundImage: `url('https://tesla-cdn.thron.com/delivery/public/image/tesla/1a5ba4b1-efd8-40bc-8b6d-e81bf8223e37/bvlatuR/std/2560x1440/Model-3-Performance-Hero-Desktop-LHD')`,
        height: "100vh",
        width: "100vw",
        backgroundPosition: "center",
      }}
    >
      <nav className="navbar navbar-expand-lg navbar-light bg-transparent">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">
            CarCar
          </NavLink>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mx-auto mb-2 mb-lg-0 ">
              <li className="nav-item dropdown">
                <NavLink
                  exact
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Sales
                </NavLink>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li>
                    <NavLink exact className="dropdown-item" to="/sales">
                      List sales
                    </NavLink>
                  </li>
                  <li>
                    <NavLink exact className="dropdown-item" to="/sales/detail">
                      Show salesperson sales
                    </NavLink>
                  </li>
                  <li>
                    <NavLink exact className="dropdown-item" to="/sales/create">
                      Register a sale
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown">
                <NavLink
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Automobile
                </NavLink>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li>
                    <NavLink
                      className="dropdown-item"
                      to="/automobiles/create/"
                    >
                      Create an Automobile
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="dropdown-item" to="/automobiles/">
                      List of Automobiles
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown">
                <NavLink
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Vehicle Model
                </NavLink>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li>
                    <NavLink className="dropdown-item" to="/models/create/">
                      Create a Model
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="dropdown-item" to="/models/">
                      List of Models
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown">
                <NavLink
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Manufacturer
                </NavLink>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li>
                    <NavLink
                      className="dropdown-item"
                      to="/manufacturers/create/"
                    >
                      Create a Manufacturer
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="dropdown-item" to="/manufacturers/">
                      List of Manufacturers
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown">
                <NavLink
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Registration
                </NavLink>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li>
                    <NavLink className="dropdown-item" to="/customer/create">
                      Register a Customer
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="dropdown-item" to="/salesperson/create">
                      Register a Salesperson
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="dropdown-item" to="/technician">
                      Register a Technician
                    </NavLink>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown">
                <NavLink
                  className="nav-link dropdown-toggle"
                  to="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Service Appointment
                </NavLink>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li>
                    <NavLink className="dropdown-item" to="/service/new/">
                      Create a New Service Appointment
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="dropdown-item" to="/service/">
                      List of Service Appointments
                    </NavLink>
                  </li>
                  <li>
                    <NavLink className="dropdown-item" to="/service/history/">
                      History of Service Appointments
                    </NavLink>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="container py-5">
        <h1 className="display-5 fw-bold">CarCar</h1>
        <p className="lead mb-4">
          The premiere solution for automobile dealership management!
        </p>
      </div>
    </div>
  );
}

export default MainPage;
