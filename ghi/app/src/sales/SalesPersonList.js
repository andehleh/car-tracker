import React, { useEffect, useState } from 'react'


function SalesPersonList() {

    const [sales, setSales] = useState([]);
    const [salesPerson, setSalesPerson] = useState([]);
    const [person, setPerson] = useState([]);
    const [personChange, setPersonChange] = useState(false);

    const fetchData = async () => {

        const url = `http://localhost:8090/api/sales/${person}`

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }

    }

    const fetchSalesPersonData = async () => {

        const url = `http://localhost:8090/api/salespeople/`

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.salespeople)
        }
    }

    const handleSalesPersonChange = (e) => {
        setPerson(e.target.value);
        setPersonChange(true)
    }

    if (personChange) {
        fetchData();
        setPersonChange(false)
    }

    useEffect(() => {
        fetchSalesPersonData();
    }, []);

    return (<>
        <h1>Salesperson History</h1>
        <select required className="form-select" onChange={handleSalesPersonChange}>
            <option value="">Select Salesperson</option>
            {salesPerson.map(person => {
                return (
                    <option key={person.id} value={person.id}>
                        {person.salesperson_name}
                    </option>
                );
            })}
        </select>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Sales person</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Sale price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.sales_person.salesperson_name}</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    </>
    )
}

export default SalesPersonList