import React, { useEffect, useState } from 'react'


function SalesForm() {

    const [autos, setAutos] = useState([]);
    const [salesPerson, setSalesPerson] = useState([]);
    const [customers, setCustomers] = useState([])
    const [auto, setAuto] = useState("");
    const [person, setPerson] = useState("");
    const [customer, setCustomer] = useState("");
    const [price, setPrice] = useState("");

    const handleSubmit = async (e) => {

        e.preventDefault();
        const data = {};

        data.customer = customer;
        data.sales_person = person;
        data.automobile = auto;
        data.price = price;

        const url = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();

            setAuto('');
            setCustomer('');
            setPerson('');
            setPrice('');
        }
    }

    const handleAutoChange = (e) => {
        setAuto(e.target.value)
    }

    const handleCustomerChange = (e) => {
        setCustomer(e.target.value)
    }

    const handlePersonChange = (e) => {
        setPerson(e.target.value)
    }

    const handlePriceChange = (e) => {
        setPrice(e.target.value)
    }

    const fetchAutoData = async () => {
        const url = "http://localhost:8090/api/autos/"

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }

    const fetchPersonData = async () => {
        const url = "http://localhost:8090/api/salespeople/"

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.salespeople)
        }
    }

    const fetchCustomerData = async () => {
        const url = "http://localhost:8090/api/customers/"

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }


    useEffect(() => {
        fetchAutoData();
        fetchCustomerData();
        fetchPersonData();
    }, [])


    return (<>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Register your sale</h1>
                    <form id="create-shoe-form" onSubmit={handleSubmit}>
                        <div className='mb-3'>
                            <select required value={auto} className="form-select" onChange={handleAutoChange}>
                                <option value="">Choose an automobile</option>
                                {autos.map(auto => {
                                    if (!(auto.sold)) {
                                        return (
                                            <option key={auto.vin} value={auto.vin}>
                                                {auto.vin}
                                            </option>
                                        );
                                    }
                                })}
                            </select>
                        </div>
                        <div className='mb-3'>
                            <select required value={person} className="form-select" onChange={handlePersonChange}>
                                <option value="">Choose a sales person</option>
                                {salesPerson.map(person => {
                                    return (
                                        <option key={person.id} value={person.id}>
                                            {person.salesperson_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className='mb-3'>
                            <select required value={customer} className="form-select" onChange={handleCustomerChange}>
                                <option value="">Choose a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                required
                                className="form-control"
                                name="price"
                                id="price"
                                type="number"
                                onChange={handlePriceChange}
                                value={price}
                            />
                            <label htmlFor="price">Sale Price</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>);
}

export default SalesForm