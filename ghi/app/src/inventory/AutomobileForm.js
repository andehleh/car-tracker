import React, { useEffect, useState } from "react";

function AutomobileForm() {
  const [models, setModels] = useState([]);
  const [model, setModel] = useState("");
  const [color, setColor] = useState("");
  const [year, setYear] = useState("");
  const [vin, setVin] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};

    data.color = color;
    data.year = year;
    data.vin = vin;
    data.model_id = model;

    const url = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newAuto = await response.json();

      setColor("");
      setYear("");
      setVin("");
      setModel("");
    }
  };

  const fetchModelData = async () => {
    const url = "http://localhost:8100/api/models/";

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  const handleColorChange = (e) => {
    setColor(e.target.value);
  };

  const handleYearChange = (e) => {
    setYear(e.target.value);
  };

  const handleVinChange = (e) => {
    setVin(e.target.value);
  };

  const handleModelChange = (e) => {
    setModel(e.target.value);
  };

  useEffect(() => {
    fetchModelData();
  }, []);

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>
            <form id="create-model-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  required
                  className="form-control"
                  name="color"
                  id="color"
                  type="text"
                  onChange={handleColorChange}
                  value={color}
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  required
                  className="form-control"
                  name="year"
                  id="year"
                  type="text"
                  onChange={handleYearChange}
                  value={year}
                />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  required
                  className="form-control"
                  name="vin"
                  id="vin"
                  type="text"
                  onChange={handleVinChange}
                  value={vin}
                />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="mb-3">
                <select
                  required
                  value={model}
                  className="form-select"
                  onChange={handleModelChange}
                >
                  <option value="">Choose a model</option>
                  {models.map((model) => {
                    return (
                      <option key={model.id} value={model.id}>
                        {model.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default AutomobileForm;
