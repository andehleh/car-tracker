import React, { useEffect, useState } from "react";

function ModelForm() {
  const [name, setName] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};

    data.name = name;
    data.picture_url = pictureUrl;
    data.manufacturer_id = manufacturer;

    const url = "http://localhost:8100/api/models/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newModel = await response.json();

      setName("");
      setPictureUrl("");
      setManufacturer("");
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/manufacturers/";

    const dataResponse = await fetch(url);
    if (dataResponse.ok) {
      const data = await dataResponse.json();
      setManufacturers(data.manufacturers);
    }
  };

  const handleManufacturerChange = (e) => {
    setManufacturer(e.target.value);
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handlePictureChange = (e) => {
    setPictureUrl(e.target.value);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form id="create-model-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  required
                  className="form-control"
                  name="name"
                  id="name"
                  type="text"
                  onChange={handleNameChange}
                  value={name}
                />
                <label htmlFor="sale_price">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  required
                  className="form-control"
                  name="picture"
                  id="picture"
                  type="text"
                  onChange={handlePictureChange}
                  value={pictureUrl}
                />
                <label htmlFor="sale_price">Picture Url</label>
              </div>
              <div className="mb-3">
                <select
                  required
                  value={manufacturer}
                  className="form-select"
                  onChange={handleManufacturerChange}
                >
                  <option value="">Choose a manufacturer</option>
                  {manufacturers.map((car) => {
                    return (
                      <option key={car.id} value={car.id}>
                        {car.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default ModelForm;
