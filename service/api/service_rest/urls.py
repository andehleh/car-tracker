from django.urls import path
from .views import api_list_services, api_show_services, api_list_technicians, get_service_history_list


urlpatterns = [
    path("services/", api_list_services, name="api_list_services"),
    path("services/<int:id>/", api_show_services, name="api_show_services"),
    path("technician/", api_list_technicians, name="api_list_technicians"),
    path("services/history/", get_service_history_list, name="api_list_history"),
]
