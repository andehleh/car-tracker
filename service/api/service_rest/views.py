from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse


from .models import Technician, Service
from .encoders import TechnicianEncoder, ServiceEncoder


@require_http_methods(["GET", "POST"])
def api_list_services(request):
    if request.method == "GET":
        service = Service.objects.all()
        return JsonResponse(
            service,
            encoder=ServiceEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Employee ID!"},
                status=400
            )
        new_service = Service.objects.create(**content)
        return JsonResponse(
            new_service,
            encoder=ServiceEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_services(request, id):
    if request.method == "GET":
        service = Service.objects.get(id=id)

        return JsonResponse(
            service,
            encoder=ServiceEncoder,
            safe=False
        )
    elif request.method == "PUT":
        Service.objects.filter(id=id).update(finished=True)
        service = Service.objects.get(id=id)
        return JsonResponse(
            service,
            encoder=ServiceEncoder,
            safe=False
        )
    else:
        try:
            count, _ = Service.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Service.DoesNotExist:
            return JsonResponse({"message": "Does Not Exist"})


@require_http_methods({"GET", "POST"})
def api_list_technicians(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technicians": technician},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Does Not Exist"},
                status=400
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})


@require_http_methods(["GET"])
def get_service_history_list(request):
    history = Service.objects.filter(finished=True)
    return JsonResponse(
        {"history": history},
        encoder=ServiceEncoder,
    )
