from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    vip = models.BooleanField(default=False)
    import_href = models.CharField(max_length=300)


class Technician(models.Model):
    technician_name = models.CharField(max_length=300)
    employee_number = models.PositiveIntegerField()


class Service(models.Model):
    vin = models.CharField(max_length=17)
    owner = models.CharField(max_length=300)
    appointment = models.DateTimeField(null=True)
    reason = models.CharField(max_length=300)
    finished = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.CASCADE,
    )

    @property
    def technician_name(self):
        return self.technician.technician_name

    def formatted_appointment(self):
        return self.appointment.strftime("%Y-%m-%d %H:%M:%S")
