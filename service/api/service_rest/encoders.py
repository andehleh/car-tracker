from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Service


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "vip",
        "id",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "technician_name",
        "employee_number",
        "id",
    ]


class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin",
        "owner",
        "appointment",
        "reason",
        "finished",
        "technician",
        "id",
    ]

    encoders = {"technician": TechnicianEncoder()}

    def get_extra_data(self, o):
        try:
            AutomobileVO.objects.get(vin=o.vin)
            return {"vip": True}
        except AutomobileVO.DoesNotExist:
            return {"vip": False}
