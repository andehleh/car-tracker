# CarCar

Team:

* Dominic - Sales microservice
* Andy - Service microservice

## Design

We have three distinct microservices that our front-end interacts with. The inventory service is responsible for managing automobiles, manufacturers, and vehicles. The sales microservice is responsible for handling salespeople, customers, and sales. Lastly, the service microservice manages service appointments and technicians.

Using React, we developed a front-end that displays data from each microservice. Additionally, the use of React allows for the creation of a responsive and user-friendly interface.

## Service microservice

I created a service api that handles all the service appointments and a poller for services that is integrated with other services. 

Service model:
The Service model manages appointments by providing functionality to list, create, update, and delete them.

Technician model:
The Technician model creates technicians and assigns them to appointments to ensure they are serviced by qualified staff and scheduled efficiently.
 
AutomobileVO model:
The AutomobileVO is used to compare VIN numbers from the inventory service's Automobile model to ensure accurate vehicle information for appointments.

## Sales microservice

Created a sales api that tracks and manages all the sales recorded in the application with a poller that pulls automobiles from the inventory api.

Added a sold property to the automobile VO model to filter the available inventory of autos.

Made a sick looking SPA that totally thrashes the competition bro. 
